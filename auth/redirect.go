package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/prometheus/common/log"
	"io/ioutil"
	"net/http"
	"time"
)

type TokenUser struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	AvatarURL string `json:"avatar_url"`
	Admin     bool   `json:"selectadiff_admin"` // This field is for internal use and not exposed by GitLab
}

func (user *TokenUser) lookupAdmin() {
	if user.ID == 555989 {
		user.Admin = true
	}
}

func Redirect(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	var user TokenUser

	code := request.URL.Query().Get("code")
	state := request.URL.Query().Get("state")
	cookie, err := request.Cookie("selectadiff_oauth_state")
	if err != nil {
		log.Error("No Cookie!")
		writer.Header().Set("Location", "/?loginFailed=true&type=cookie")
		writer.WriteHeader(http.StatusFound)
		return
	}
	if cookie.Value != state {
		log.Error("Illegal state!")
		writer.Header().Set("Location", "/?loginFailed=true&type=state")
		writer.WriteHeader(http.StatusFound)
		return
	}

	ctx := context.Background()

	tok, err := conf.Exchange(ctx, code)
	if err != nil {
		writer.Header().Set("Location", "/?loginFailed=true&type=exchange")
		writer.WriteHeader(http.StatusFound)
		return
	}

	client := conf.Client(ctx, tok)
	resp, err := client.Get("https://gitlab.com/api/v4/user")
	if err != nil {
		log.Error(err)
		writer.Header().Set("Location", "/?loginFailed=true&type=api_failed")
		writer.WriteHeader(http.StatusFound)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Errorf("Response != 200: %d", resp.StatusCode)
		writer.Header().Set("Location", fmt.Sprintf("/?loginFailed=true&type=%s", resp.StatusCode))
		writer.WriteHeader(http.StatusFound)
		return
	}

	data, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(data, &user)
	if err != nil {
		log.Error(err)
		writer.Header().Set("Location", "/?loginFailed=true&type=json_response")
		writer.WriteHeader(http.StatusFound)
		return
	}

	authToken, err := createJWT(user)
	if err != nil {
		log.Error(err)
		writer.Header().Set("Location", "/?loginFailed=true&type=jwt")
		writer.WriteHeader(http.StatusFound)
		return
	}

	authCookie := http.Cookie{Name: "selectadiff_auth", Path: "/", Value: authToken, HttpOnly: true, SameSite: http.SameSiteLaxMode}
	stateCookie := http.Cookie{Name: "selectadiff_oauth_state", Path: "/", Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}
	writer.Header().Add("Set-Cookie", authCookie.String())
	writer.Header().Add("Set-Cookie", stateCookie.String())
	writer.Header().Set("Location", "/")
	writer.WriteHeader(http.StatusFound)
}
