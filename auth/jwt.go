package auth

import (
	"encoding/json"
	"gopkg.in/square/go-jose.v2"
	"os"
)

var key []byte
var sig jose.Signer
var sKey jose.SigningKey

func init() {
	if url := os.Getenv("JWT_SECRET"); url != "" {
		key = []byte(url)
	} else {
		key = []byte("development")
	}
	sKey = jose.SigningKey{Algorithm: jose.HS512, Key: key}
	var err error
	sig, err = jose.NewSigner(sKey, (&jose.SignerOptions{}).WithType("JWT"))
	if err != nil {
		panic(err)
	}
}

func createJWT(user TokenUser) (string, error) {
	j, err := json.Marshal(user)
	if err != nil {
		return "", err
	}
	raw, err := sig.Sign(j)
	if err != nil {
		return "", err
	}
	return raw.CompactSerialize()
}
