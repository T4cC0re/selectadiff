package auth

import (
	"github.com/google/uuid"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/selectadiff/flags"
	"gitlab.com/T4cC0re/selectadiff/util"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"
	"gopkg.in/square/go-jose.v2/jwt"
	"net/http"
	"strings"
	"time"
)

var conf = &oauth2.Config{
	ClientID:    *flags.ClientID,
	Scopes:      []string{"read_user"},
	Endpoint:    gitlab.Endpoint,
	RedirectURL: *flags.RedirectURL,
}

func Login(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	state := uuid.New().String()

	url := conf.AuthCodeURL(state, oauth2.AccessTypeOnline)

	stateCookie := http.Cookie{Name: "selectadiff_oauth_state", Path: "/", Value: state, HttpOnly: true, SameSite: http.SameSiteLaxMode}
	authCookie := http.Cookie{Name: "selectadiff_auth", Path: "/", Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}

	writer.Header().Add("Set-Cookie", authCookie.String())
	writer.Header().Add("Set-Cookie", stateCookie.String())
	writer.Header().Set("Location", url)
	writer.WriteHeader(http.StatusFound)
}

func UserByRequest(request *http.Request) *TokenUser {
	var token string
	cookie, err := request.Cookie("selectadiff_auth")
	if err != nil {
		bearer := request.Header.Get("Authorization")
		split := strings.Split(bearer, " ")
		if len(split) < 2 {
			return nil
		}
		token = split[1]
	} else {
		token = cookie.Value
	}

	tok, err := jwt.ParseSigned(token)
	if err != nil {
		log.Error(err)
		return nil
	}

	var user TokenUser
	err = tok.Claims(key, &user)
	if err != nil {
		log.Error(err)
		return nil
	}
	user.lookupAdmin()
	return &user
}

func Check(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	user := UserByRequest(request)
	if user == nil {
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	util.WriteJSON(writer, user, true)
}

func Logout(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	authCookie := http.Cookie{Name: "selectadiff_auth", Path: "/", Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}
	stateCookie := http.Cookie{Name: "selectadiff_oauth_state", Path: "/", Expires: time.Unix(0, 0), HttpOnly: true, SameSite: http.SameSiteLaxMode}

	writer.Header().Add("Set-Cookie", stateCookie.String())
	writer.Header().Add("Set-Cookie", authCookie.String())
	writer.Header().Set("Location", "/?logout=true")
	writer.WriteHeader(http.StatusFound)
}
