module gitlab.com/T4cC0re/selectadiff

go 1.14

require (
	github.com/go-bindata/go-bindata/v3 v3.1.3 // indirect
	github.com/google/uuid v1.6.0
	github.com/gorilla/mux v1.8.1
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prologic/bitcask v0.3.10
	github.com/prometheus/common v0.9.1
	golang.org/x/oauth2 v0.12.0
	gopkg.in/square/go-jose.v2 v2.6.0
	gorm.io/driver/sqlite v1.5.6
	gorm.io/gorm v1.25.12
)
