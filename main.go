package main

//go:generate go run generators/frontend.go

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/selectadiff/auth"
	"gitlab.com/T4cC0re/selectadiff/database"
	"gitlab.com/T4cC0re/selectadiff/flags"
	"gitlab.com/T4cC0re/selectadiff/git"
	"gitlab.com/T4cC0re/selectadiff/plumbing"
	"gitlab.com/T4cC0re/selectadiff/userdata"
	"gitlab.com/T4cC0re/selectadiff/util"
	"io"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strconv"
	"strings"
	"syscall"
	"time"
)

var version = "source"

func setHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Referrer-Policy", "strict-origin")
		w.Header().Set("Cross-Origin-Resource-Policy", "same-origin")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-Frame-Options", "DENY")
		w.Header().Set("L1-cache", "NO")
		next.ServeHTTP(w, req)
	})
}

func maintenanceTimer() {
	for {
		var repos []*git.Repo
		database.DB.Find(&repos)
		for _, repo := range repos {
			err := git.GitPull(repo, os.Stdout)
			log.Error(err)
			log.With("repo", repo.Name).Info("repo updated")
			err = git.WriteCommitGraph(repo, os.Stdout)
			log.Error(err)
		}
		time.Sleep(5 * time.Minute)
		//_flushCache()
	}
}

func main() {
	err := database.Open()
	if err != nil {
		log.Fatal(err)
	}

	err = database.DB.AutoMigrate(&git.Repo{}, &userdata.UserData{})
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if r := recover(); r != nil {
			database.Sync()
		}
	}()
	defer database.Sync()
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGSTOP, syscall.SIGINT, syscall.SIGHUP)
	go func() {
		<-c
		database.Sync()
		os.Exit(0)
	}()

	r := mux.NewRouter()
	r.Use(setHeaders)
	// TODO https://github.com/gorilla/mux#registered-urls
	r.Path("/api/v1/admin/cache/flush").Methods("GET", "POST").HandlerFunc(flushCache)
	r.Path("/api/v1/admin/repo/{repoId:[0-9]+}/update").Methods("GET", "POST").HandlerFunc(repoUpdate)
	r.Path("/api/v1/admin/repo/new").Methods("POST").HandlerFunc(repoClone)
	r.Path("/api/v1/repo/{repoId:[0-9]+}/commit/before/date/{date:[0-9]{4}-[0-9]{2}-[0-9]{2}}").Methods("GET").HandlerFunc(commitByDate)
	r.Path("/api/v1/repo/{repoId:[0-9]+}/commit/by/hash/{commit:[a-z0-9]+}").Methods("GET").HandlerFunc(getCommit)
	r.Path("/api/v1/repo/{repoId:[0-9]+}/commit/current").Methods("GET").HandlerFunc(currentCommit)
	r.Path("/api/v1/repo/{repoId:[0-9]+}/commit/first").Methods("GET").HandlerFunc(firstCommit)
	r.Path("/api/v1/repos").Methods("GET").HandlerFunc(listRepos)
	r.Path("/api/oauth/redirect").Methods("GET").HandlerFunc(auth.Redirect)
	r.Path("/api/v1/auth/check").Methods("GET").HandlerFunc(auth.Check)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}").Methods("GET").HandlerFunc(getUser)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/prefixes").Methods("GET").HandlerFunc(getPrefixes)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/prefixes").Methods("PUT").HandlerFunc(putPrefixes)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/last_commit").Methods("GET").HandlerFunc(getLastCommit)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/last_commit").Methods("PUT").HandlerFunc(putLastCommit)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/changes/from/{commit:[a-z0-9]+}").Methods("GET").HandlerFunc(userChanges)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/diffs/from/{commit:[a-z0-9]+}").Methods("GET").HandlerFunc(userDiffs)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/changes/from/{commit:[a-z0-9]+}/to/{target:[a-z0-9]+}").Methods("GET").HandlerFunc(userChanges)
	r.Path("/api/v1/user/{userId:[0-9]+}/repo/{repoId:[0-9]+}/diffs/from/{commit:[a-z0-9]+}/to/{target:[a-z0-9]+}").Methods("GET").HandlerFunc(userDiffs)
	r.Path("/api/version").Methods("GET").HandlerFunc(showVersion)
	r.Path("/api/v1/my/export").Methods("GET").HandlerFunc(userExport)
	r.Path("/login").Methods("GET").HandlerFunc(auth.Login)
	r.Path("/logout").Methods("GET").HandlerFunc(auth.Logout)
	r.PathPrefix("/api/v1/repo/{repoId:[0-9]+}/content").Methods("GET").HandlerFunc(repoContent)
	r.PathPrefix("/api/v1/my/repo/{repoId:[0-9]+}").HandlerFunc(userRedir)
	r.PathPrefix("/").Methods("GET").Handler(http.FileServer(AssetFile()))
	http.Handle("/", r)
	go maintenanceTimer()
	log.Infof("SelectaDiff %s up and running", version)
	if os.Getenv("TLS_CERT") != "" && os.Getenv("TLS_KEY") != "" {
		log.Error(http.ListenAndServeTLS(":8081", os.Getenv("TLS_CERT"), os.Getenv("TLS_KEY"), nil))
	} else {
		log.Error(http.ListenAndServe(":8081", nil))
	}
}

func userExport(w http.ResponseWriter, r *http.Request) {
	currentUser := auth.UserByRequest(r)
	var users []*userdata.UserData

	type RE struct {
		Name           string
		Upstream       string
		TrackingBranch string
		LastReadCommit string
		FilterPrefixes []string
	}

	type UE struct {
		GitLabUserID int64
		RepoData     []RE
	}

	database.DB.Preload("Repo").Find(&users, userdata.UserData{GitLabUserId: currentUser.ID})
	setNoCache(w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var re []RE
	for _, user := range users {
		re = append(re, RE{
			Name:           user.Repo.Name,
			Upstream:       user.Repo.Upstream,
			TrackingBranch: user.Repo.TrackingBranch,
			LastReadCommit: user.LastCommitHash,
			FilterPrefixes: user.FilterPrefixes,
		})
	}

	_ = json.NewEncoder(w).Encode(UE{
		GitLabUserID: currentUser.ID,
		RepoData:     re,
	})
}

func listRepos(w http.ResponseWriter, e *http.Request) {
	var repos []*git.Repo
	database.DB.Find(&repos)
	setNoCache(w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(repos)
}

func repoContent(w http.ResponseWriter, r *http.Request) {
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	dirList, err := git.List(repo, strings.Replace(r.URL.Path, "/api/v1/repo/"+repoId+"/content", ".", 1))
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(&dirList)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) && !isAdmin(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	userId := mux.Vars(r)["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	w.Header().Set("Content-Type", "application/json")

	user, err := userdata.GetOrCreateUserData(userId, repo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_ = json.NewEncoder(w).Encode(&user)
}

type LastCommitPayload struct {
	LastCommitHash string `json:"last_commit_hash"`
}

type FilterPrefixPayload struct {
	FilterPrefixes []string `json:"filter_prefixes"`
}

func putLastCommit(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	userId := mux.Vars(r)["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	w.Header().Set("Content-Type", "application/json")
	var payload LastCommitPayload
	_ = json.NewDecoder(r.Body).Decode(&payload)

	if payload.LastCommitHash == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	user, err := userdata.GetOrCreateUserData(userId, repo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	user.LastCommitHash = payload.LastCommitHash
	err = database.DB.Save(&user).Error
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_ = json.NewEncoder(w).Encode(&payload)
}

func getLastCommit(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	userId := mux.Vars(r)["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	log.Infof("%+v", repoId)
	log.Infof("%+v", repo)
	w.Header().Set("Content-Type", "application/json")

	user, err := userdata.GetOrCreateUserData(userId, repo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("/api/v1/repo/%d/commit/by/hash/%s", repo.ID, user.LastCommitHash))
	w.WriteHeader(http.StatusFound)
}

func setImmutableCache(w http.ResponseWriter) {
	w.Header().Set("cache-Control", "public, max-age=31536000, immutable")
}

func ETagMatch304(r *http.Request, w http.ResponseWriter, userData *userdata.UserData) bool {
	if userData == nil {
		return false
	}

	setEtag(w, userData)

	if r.Header.Get("If-None-Match") == userData.ETag() {
		log.Infof("ETag matched!")
		w.WriteHeader(http.StatusNotModified)
		return true
	}
	return false
}

func setEtag(w http.ResponseWriter, userData *userdata.UserData) {
	if userData == nil {
		setNoCache(w)
		return
	}
	w.Header().Set("ETag", userData.ETag())
	w.Header().Set("cache-Control", "public, max-age=10, must-revalidate")
}

func setNoCache(w http.ResponseWriter) {
	w.Header().Set("cache-Control", "private, max-age=0, no-store")
}

func setL1CacheHit(w http.ResponseWriter) {
	w.Header().Set("L1-cache", "HIT")
}

func setL1CacheMiss(w http.ResponseWriter) {
	w.Header().Set("L1-cache", "MISS")
}

func getCommit(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	vars := mux.Vars(r)
	commitHash := vars["commit"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)

	commit, err := git.CommitDetails(repo, commitHash)
	if err != git.ErrNoCommit && err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	} else {
		if commit == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		setImmutableCache(w)
		setL1CacheMiss(w)
		util.WriteJSON(w, commit, true)
	}
}

func userRedir(writer http.ResponseWriter, request *http.Request) {
	setNoCache(writer)
	user := auth.UserByRequest(request)
	if user == nil || user.ID == 0 {
		log.Info(user)
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	writer.Header().Set("Location", strings.Replace(request.URL.EscapedPath(), "/api/v1/my", fmt.Sprintf("/api/v1/user/%d", user.ID), 1)+"?"+request.URL.RawQuery)
	writer.WriteHeader(http.StatusFound)
}

func putPrefixes(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	userId := mux.Vars(r)["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	w.Header().Set("Content-Type", "application/json")
	var payload FilterPrefixPayload
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	log.Infof("%+v", payload)

	if len(payload.FilterPrefixes) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	user, err := userdata.GetOrCreateUserData(userId, repo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	user.FilterPrefixes = payload.FilterPrefixes
	err = database.DB.Save(&user).Error
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(&payload)
}

func getPrefixes(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	userId := mux.Vars(r)["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	w.Header().Set("Content-Type", "application/json")

	user, err := userdata.GetOrCreateUserData(userId, repo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	p := FilterPrefixPayload{user.FilterPrefixes}

	json.NewEncoder(w).Encode(&p)
}

func showVersion(writer http.ResponseWriter, _ *http.Request) {
	type VersionInfo struct {
		Version string `json:"version"`
		Product string `json:"product"`
	}
	setNoCache(writer)
	util.WriteJSON(writer, VersionInfo{Version: version, Product: "selectadiff"}, true)
}

func isAdmin(r *http.Request) bool {
	user := auth.UserByRequest(r)
	return user != nil && user.Admin
}

func isRightUser(r *http.Request) bool {
	userId := mux.Vars(r)["userId"]
	user := auth.UserByRequest(r)
	return user != nil && strconv.FormatInt(user.ID, 10) == userId
}

type CmdLogger struct {
	Writer io.Writer
}

func (l CmdLogger) Write(p []byte) (n int, err error) {
	n, err = l.Writer.Write(p)
	if err != nil {
		return len(p), nil
	}
	_, _ = os.Stderr.Write(p)
	return
}

func repoClone(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Add("Transfer-Encoding", "chunked")
	setNoCache(writer)

	if !isAdmin(r) {
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	var payload git.Repo
	var err error
	err = json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		log.Error(err)
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	logger := CmdLogger{Writer: writer}

	tmp := sha256.Sum256([]byte(payload.Upstream))
	upstreamHash := hex.EncodeToString(tmp[:])
	var repo git.Repo
	repo.Upstream = payload.Upstream
	if payload.Name == "" {
		repo.Name = payload.Upstream
	} else {
		repo.Name = payload.Name
	}
	repo.Path = path.Join(*flags.StoragePath, upstreamHash[:])
	if payload.TrackingBranch == "" {
		repo.TrackingBranch, err = git.DetectDefaultBranch(payload.Upstream)
	} else {
		repo.TrackingBranch = payload.TrackingBranch
	}

	if err != nil {
		log.Error(err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = database.DB.Create(&repo).Error
	log.Error(err)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	} else {
		writer.WriteHeader(http.StatusOK)
	}

	err = database.DB.Save(&repo).Error
	log.Error(err)

	err = git.GitClone(&repo, logger)
	log.Error(err)
	err = git.WriteCommitGraph(&repo, logger)
	log.Error(err)
}

func flushCache(writer http.ResponseWriter, r *http.Request) {
	setNoCache(writer)
	if !isAdmin(r) {
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	writer.WriteHeader(http.StatusOK)

	_flushCache()
}

func _flushCache() {
	plumbing.ChangeCache.Reset(*flags.CacheSize)
}

func repoUpdate(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Add("Transfer-Encoding", "chunked")
	setNoCache(writer)

	if !isAdmin(r) {
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)

	writer.WriteHeader(http.StatusOK)

	logger := CmdLogger{Writer: writer}

	err := git.GitPull(repo, logger)
	log.Error(err)
	err = git.WriteCommitGraph(repo, logger)
	log.Error(err)
}

func currentCommit(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	commit, err := git.GetCurrentCommit(repo)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.Header().Set("Location", fmt.Sprintf("/api/v1/repo/%d/commit/by/hash/%s", repo.ID, commit))
		w.WriteHeader(http.StatusFound)
	}
}

func firstCommit(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	log.Info("First commit")
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	commit, err := git.GetFirstCommit(repo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.Header().Set("Location", fmt.Sprintf("/api/v1/repo/%d/commit/by/hash/%s", repo.ID, commit))
		w.WriteHeader(http.StatusFound)
	}
}

func commitByDate(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	vars := mux.Vars(r)
	dateString := vars["date"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	date, err := time.Parse("2006-01-02", dateString)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	commit, err := git.CommitByDate(repo, date)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.Header().Set("Location", fmt.Sprintf("/api/v1/repo/%d/commit/by/hash/%s", repo.ID, commit))
		w.WriteHeader(http.StatusFound)
	}
}

func userDiffs(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	vars := mux.Vars(r)
	var unfiltered bool
	var sOffset string
	var sCount string
	var offset int64
	var count int64
	_, unfiltered = r.URL.Query()["unfiltered"]
	sOffset = r.URL.Query().Get("offset")
	sCount = r.URL.Query().Get("count")
	target := vars["target"]
	commit := vars["commit"]
	userId := vars["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	if target == "" {
		// If no target is specified, let's redirect the user to the version with the current target hash
		var err error
		target, err = git.GetCurrentCommit(repo)
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.Header().Set("Location", fmt.Sprintf("/api/v1/user/%s/repo/%d/diffs/from/%s/to/%s?%s", userId, repo.ID, commit, target, r.URL.RawQuery))
		w.WriteHeader(http.StatusFound)
		return
	}

	userData, err := userdata.GetOrCreateUserData(vars["userId"], repo)
	if err != nil {
		return
	}

	if ETagMatch304(r, w, userData) {
		return
	}

	changeSet, _, err := plumbing.ChangesFromCommit(repo, vars["commit"], target)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if unfiltered {
		// If we have a target hash and we are not requesting filtered results, the response is immutable
		setImmutableCache(w)
	} else {
		var ucs *plumbing.UserChangeSet

		ucs, err = plumbing.FilterChangeset(userData, changeSet)
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		changeSet = &ucs.ChangeSet
	}
	if changeSet == nil {
		log.Error("Changeset became nil")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	offset, _ = strconv.ParseInt(sOffset, 10, 32)
	count, _ = strconv.ParseInt(sCount, 10, 32)

	err = plumbing.DiffFromChanges(changeSet, int(count), int(offset), w, r.URL)
	if err != nil {
		log.Error(err)
	}
}

func userChanges(w http.ResponseWriter, r *http.Request) {
	setNoCache(w)
	if !isRightUser(r) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	vars := mux.Vars(r)
	var pretty, showChanges, unfiltered bool
	_, pretty = r.URL.Query()["pretty"]
	_, showChanges = r.URL.Query()["showChanges"]
	_, unfiltered = r.URL.Query()["unfiltered"]
	target := vars["target"]
	commit := vars["commit"]
	userId := vars["userId"]
	repoId := mux.Vars(r)["repoId"]
	repo := git.GetRepoByID(repoId)
	log.Infof("%+v", repo)
	if target == "" {
		// If no target is specified, let's redirect the user to the version with the current target hash
		var err error
		target, err = git.GetCurrentCommit(repo)
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		w.Header().Set("Location", fmt.Sprintf("/api/v1/user/%s/repo/%d/changes/from/%s/to/%s?%s", userId, repo.ID, commit, target, r.URL.RawQuery))
		w.WriteHeader(http.StatusFound)
		return
	}

	userData, err := userdata.GetOrCreateUserData(vars["userId"], repo)
	if err != nil {
		return
	}

	if ETagMatch304(r, w, userData) {
		return
	}

	changeSet, cacheHit, err := plumbing.ChangesFromCommit(repo, commit, target)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if cacheHit {
		setL1CacheHit(w)
	} else {
		setL1CacheMiss(w)
	}

	if unfiltered {
		// If we have a target hash and we are not requesting filtered results, the response is immutable
		setImmutableCache(w)
	} else {
		var ucs *plumbing.UserChangeSet

		ucs, err = plumbing.FilterChangeset(userData, changeSet)
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		changeSet = &ucs.ChangeSet
	}

	if changeSet == nil {
		log.Error("Changeset became nil")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if !showChanges {
		// Shallow copy without changes. Cannot set to nil without corrupting cache.
		changeSet = &plumbing.ChangeSet{
			//UserId:   changeSet.UserId,
			Count:   changeSet.Count,
			Commit:  changeSet.Commit,
			Target:  changeSet.Target,
			Repo:    changeSet.Repo,
			Changes: nil,
			//Filtered: changeSet.Filtered,
		}
	}
	util.WriteJSON(w, changeSet, pretty)
}
