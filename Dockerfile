FROM ubuntu:22.04
LABEL Maintainer="Hendrik Meyer <code@t4cc0.re>"

RUN apt update && apt install -y ca-certificates curl git && rm -rf /var/lib/apt/lists && mkdir -p /app/repo
VOLUME /app/repo
WORKDIR /app
ENTRYPOINT ["/app/selectadiff"]
EXPOSE 8081
COPY bin/selectadiff /app/selectadiff
