let userId = 0;
let userName = "";
let userAvatar = "";
let userIsAdmin = false;
let changes = null;
let repoId = parseInt(window.localStorage.getItem("repoId") || 1);
const API_PREFIX = "/api/v1"

const changeRepo = (id) => {
    repoId = id;
    window.localStorage.setItem("repoId", repoId);
    window.location.hash = "#repoChange";
}

const addPresetRepo = async () => {
    await cloneRepo("https://gitlab.com/gitlab-com/www-gitlab-com.git", "GitLab Handbook");
    await cloneRepo("https://gitlab.com/gitlab-com/runbooks.git", "GitLab On-call Run Books");
}

const nav_repoChange = async () => {
    const newRepo = parseInt(document.getElementById("sidebar_repo").value)
    if (!!newRepo && newRepo != repoId) {
        changeRepo(newRepo)
    }
}

const updateNavbar = () => {
    document.getElementById("user_avatar").setAttribute("src", userAvatar);
    document.getElementById("user_name").innerText = userName;
};

const flushCache = async () => {
    showSpinner();
    setSpinnerText("Flushing cache...");
    let output = await blobRequest(`${API_PREFIX}/admin/cache/flush`, false);
    showModal("Flush cache", await output.text(), false);
    hideSpinner();
};

const listRepos = async () => {
    let res = await fetch(`${API_PREFIX}/repos`, {cache: 'default'});
    const repos = await res.json();
    let newHTML = ""

    for (let repo of repos) {
        newHTML += `<option value="${repo.id}" ${repo.id == repoId ? "selected" : ""}>${repo.name}</option>`
    }

    document.getElementById("sidebar_repo").innerHTML = newHTML
}

const updateRepo = async () => {
    showSpinner();
    setSpinnerText("Updating repo...");
    let output = await blobRequest(`${API_PREFIX}/admin/repo/${repoId}/update`, false);
    showModal("Update repo", await output.text(), false);
    hideSpinner();
};

const cloneRepo = async (upstream, name) => {
    showSpinner();
    setSpinnerText("Cloning repo...");
    try {
        const response = await fetch(`${API_PREFIX}/admin/repo/new`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(
                {
                    upstream,
                    name,
                }
            )
        });
        showModal("Clone repo", await response.text(), false);
    } catch (e) {
        console.log(e);
    }
    hideSpinner();
};

const clearOutput = () => {
    document.getElementById("output").innerHTML = "";
    document.getElementById("cmd_output").innerText = "";
};

const convertToTree = (path, selected) => {
    const splits = path.split('/');

    let objs = [];

    for (const split in splits) {
        const obj = {};
        if (split >= 1) {
            obj.id = objs[split - 1].id + "/" + splits[split];
            obj.parent = objs[split - 1].id;
        } else {
            obj.id = splits[split];
            obj.parent = "#";
        }
        obj.text = splits[split];
        obj.state = {};
        if (split == (splits.length - 1)) {
            obj.state.selected = selected
        } else {
            obj.state.opened = true;
        }
        objs.push(obj)
    }

    return objs;
};

const filterTree = (tree) => {
    const map = new Map();
    for (obj of tree.reverse()) {
        map.set(obj.id, obj);
    }
    return Array.from(map.values());
};

const loadPrefixes = async () => {
    showSpinner();
    setSpinnerText("Loading...");
    try {
        const prefixes = await getPrefixes();
        let tree = [];
        for (let dir of prefixes.filter_prefixes) {
            console.log(dir);
            let selected = true;
            if (dir.startsWith('!')) {
                dir = dir.substr(1);
                selected = false
            }

            tree.push(...convertToTree(dir, selected));
        }
        tree = filterTree(tree);
        $('#jstree_prefixes').jstree({
            "plugins": ["wholerow", "checkbox"],
            "core": {
                "themes": {
                    "variant": "large"
                },
                'data': tree,
            },
        });
    } catch (e) {
        console.log(e)
    }

    hideSpinner();
    feather.replace();
};

const markAsRead = async () => {
    if (confirm(`Are you sure you want to mark commit ${currentCommit.hash} (commited ${(new Date(currentCommit.authoring_time * 1000)).toISOString()}) as read? You will only see new diffs after this commit.`)) {

        showSpinner();
        setSpinnerText(`Saving...`);
        await putLastCommit(currentCommit.hash);
        fromCommit = currentCommit;
        await reload();
        hideSpinner();
        return reload();
    } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
    }
};

const reload = async () => {
    changeRepo(repoId);
};

const diffByDate = async (dateString) => {
    return diffByURL(`${API_PREFIX}/repo/${repoId}/commit/before/date/${dateString}`)
};

const diffSinceLastReadCommit = async () => {
    return diffByURL(`${API_PREFIX}/my/repo/${repoId}/last_commit`)
};

const diffByCommitHash = async (commit) => {
    return diffByURL(`${API_PREFIX}/repo/${repoId}/commit/by/hash/${commit}`)
};

const addSourceElement = (path) => {

    let prefix_class = "info";
    if (path.startsWith("!")) {
        prefix_class = "danger";
    } else {
        prefix_class = "success";
    }


    const div = document.createElement("div");
    div.className = "selectadiff-prefix input-group mb-3 input-group-sm";
    div.dataset.prefix = path;
    div.innerHTML = `
        <div class="input-group-prepend">
            <div class="input-group-text"><input type="checkbox"></div>
        </div>
        <span class="input-group-text list-group-item-${prefix_class}">${path}</span>

        <div class="input-group-btn input-group-append">
            <button class="btn btn-danger">blacklist</button>
        </div>`;

    document.getElementById("output").append(div);

};

const diffByURL = async (url) => {
    clearOutput();
    showSpinner();
    setSpinnerText(`Fetching commit...`);
    try {
        let res = await fetch(url, {cache: 'default'});
        const commit = await res.json();
        console.log("commit:", commit);
        commits.set(commit.hash, commit);
        setSpinnerText(`Fetching changes since commit ${commit.hash} (${(new Date(commit.authoring_time * 1000)).toISOString()})...`);
        const changes = await getChanges(commit.hash, null);
        if (!changes) {
            console.log("No changes for commit", commit);
            hideSpinner();
            return;
        }
        await displayDiffs(changes);
        // Dance around to scroll to a hash that might have been a diff2html anchor
        let oldHash = window.location.hash;
        window.location.hash = "#diffs";
        window.location.hash = oldHash;
    } catch (e) {
        console.log(e);
        console.log("No commit for url", url);
        await displayPartial("no_commit");
    }
    hideSpinner();
};

const blobRequest = async (url, writeOutput) => {
    let blob = new Blob();
    try {
        while (url) {
            let res = await fetch(url, {cache: 'default'});
            url = res.headers.get("X-Next");
            blob = new Blob([blob, await res.blob()], {type: res.headers.get("Content-Type")});
            console.log("next url", url);
        }
        if (writeOutput) {
            clearOutput();
            document.getElementById("cmd_output").innerText = await blob.text()
        }
        return blob
    } catch (e) {
        return "";
        console.log(e);
    }
};

const getChanges = async (from, to) => {
    let url = "";
    if (to) {
        url = `${API_PREFIX}/my/repo/${repoId}/changes/from/${from}/to/${to}?showChanges`
    } else {
        url = `${API_PREFIX}/my/repo/${repoId}/changes/from/${from}?showChanges`
    }
    try {
        let res = await fetch(url, {cache: 'default'});
        const changes = await res.json();
        console.log(changes);
        return changes;
    } catch (e) {
        console.log(e);
    }
};

const hideModal = () => {
    $("#modal").modal('hide');
    document.getElementById("modal_cmd_output").innerText = "";
    document.getElementById("modal_output").innerHTML = "";
    document.getElementById("modal_title").innerText = "";
};

const showModal = (title, content, isHTML) => {
    hideModal();
    document.getElementById("modal_title").innerText = title;
    if (isHTML) {
        document.getElementById("modal_output").innerHTML = content;
        document.getElementById("modal_cmd_output").innerText = "";
    } else {
        document.getElementById("modal_output").innerHTML = "";
        document.getElementById("modal_cmd_output").innerText = content;
    }
    $("#modal").modal('show');
    feather.replace();
};

const commits = new Map();
const getCommit = async (hash) => {
    if (commits.has(hash)) {
        console.log("commit from local cache", hash);
        return commits.get(hash)
    }
    showSpinner();
    try {
        let res = await fetch(`${API_PREFIX}/repo/${repoId}/commit/by/hash/${hash}`, {cache: 'no-store'});
        const commit = await res.json();
        console.log("getCommit", commit);
        commits.set(commit.hash, commit);
        hideSpinner();
        return commit;
    } catch (e) {
        console.log(e)
    }
    hideSpinner();
};

let diff = new Blob();
let fromCommit = null;
let currentCommit = null;
const displayDiffs = async (in_changes) => {
    showSpinner();
    clearOutput();
    fromCommit = await getCommit(in_changes.from_commit);
    currentCommit = await getCommit(in_changes.to_commit);
    setSpinnerText(`Fetching diff between ${fromCommit.hash} and ${currentCommit.hash}...`);
    changes = in_changes;
    let renamed = [];
    if (!changes.changes) {
        console.log("WARN! Changes fetched without diff list");
        document.getElementById("diff_renamed").innerText = "! no data !";
    } else {
        for (const change of changes.changes) {
            if (change.rename_target) {
                console.log(change.rename_target);
                renamed.push(change);
            }
        }
    }
    document.getElementById("diff_count").innerText = in_changes.count;
    document.getElementById("diff_renamed").innerText = renamed.length;
    document.getElementById("diff_since").innerText = (new Date(fromCommit.authoring_time * 1000)).toISOString();
    document.getElementById("diff_from_time").innerText = (new Date(fromCommit.authoring_time * 1000)).toISOString();
    document.getElementById("diff_to_time").innerText = (new Date(currentCommit.authoring_time * 1000)).toISOString();
    document.getElementById("diff_from_hash").innerText = fromCommit.hash;
    document.getElementById("diff_to_hash").innerText = currentCommit.hash;
    const url = `${API_PREFIX}/my/repo/${repoId}/diffs/from/${in_changes.from_commit}/to/${in_changes.to_commit}`;
    diff = await blobRequest(url, false);
    console.log("Fetched diff with", diff.length, "bytes");
    let rawDiffs = false;
    if (changes.changes && changes.changes.length > 50) {
        if (confirm(`You are about to parse ${changes.changes.length} diffs. This can make your browser slow. We will show you the unparsed diff instead.If you are *really* sure you want to parse them all, click cancel.`)) {
            rawDiffs = true
        }
    }
    if (changes.changes && changes.changes.length > 500) {
        if (confirm(`More than 500 changes are unsupported. Click cancel to continue regardless, OK to abort.`)) {
            hideSpinner();
            return
        }
    }
    if (renamed.length > 0) {
        let alertMessage = `<div class="alert alert-danger" role="alert"><h4 class="alert-heading">Attention! These files have been renamed. Check your prefixes to ensure further updates!</h4><hr>`;
        if (rawDiffs) {
            alertMessage += `<pre>`
            for (const change of renamed) {
                alertMessage += `${change.file}\t>>\t${change.rename_target}\n`
            }
            alertMessage += `</pre></div>`
        } else {
            alertMessage += `<ul>`
            for (const change of renamed) {
                alertMessage += `<li>${change.file}&nbsp;<span data-feather="chevrons-right"></span>&nbsp;${change.rename_target}</li>`
            }
            alertMessage += `</ul></div>`
        }
        document.getElementById("output").innerHTML = alertMessage
    }

    if (rawDiffs) {
        document.getElementById("cmd_output").innerText += await diff.text();
    } else {
        setSpinnerText(`Parsing diff...`);
        document.getElementById("output").innerHTML += await Diff2Html.html(await diff.text());
    }
    feather.replace();
    hideSpinner();
};

const downloadDiff = async () => {
    return downloadBlob(diff, `selectadiff_${changes.from_commit}_-_${changes.to_commit}.diff`)
};

const downloadBlob = async (blob, filename) => {
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
};

const attemptLogin = async () => {
    showSpinner();
    setSpinnerText(`Checking login...`);
    try {
        let res = await fetch(`${API_PREFIX}/auth/check`, {cache: 'no-store'});
        const user = await res.json();
        console.log(user);
        userId = user.id;
        userName = user.name;
        userAvatar = user.avatar_url;
        userIsAdmin = user.selectadiff_admin;
        document.getElementById("admin_tools").hidden = !userIsAdmin;
        updateNavbar();
        hideSpinner();
        return true;
    } catch (e) {
        console.log(e);
        if (window.location.search.includes("logout=true") || window.location.search.includes("loginFailed=true")) {
            // This is a logout/failed login. Do not automatically re-log in
        } else {
            window.location.pathname = "/login";
        }
        hideSpinner();
        return false;
    }
};

const app = async () => {
    console.log("Starting SelectaDiff");
    $('#content').on('change keyup keydown paste cut', 'textarea', function () {
        $(this).height(0).height(this.scrollHeight);
    }).find('textarea').change();
    let loggedIn = await attemptLogin();
    if (!loggedIn) {
        return
    }
    await listRepos();
    window.onhashchange = route;
    route();
};

async function displayPartial(partial) {
    try {
        let res = await fetch(`/${partial}.partial.html`);
        document.getElementById("partial").innerHTML = await res.text();
    } catch (e) {
        console.log(e);
        document.getElementById("partial").innerHTML = "";
    }
    feather.replace();
}

const putLastCommit = async (hash) => {
    showSpinner();
    try {
        const response = await fetch(`${API_PREFIX}/my/repo/${repoId}/last_commit`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"last_commit_hash": hash}),
            cache: 'no-store'
        });
        let json = await response.json();
        console.log(json);
    } catch (e) {
        console.log(e);
    }
    hideSpinner();
};

const putPrefixes = async (data) => {
    showSpinner();
    hideModal();
    try {
        const response = await fetch(`${API_PREFIX}/my/repo/${repoId}/prefixes`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data,
            cache: 'no-store'
        });
        let json = await response.json();
        console.log(json);
    } catch (e) {
        console.log(e);
    }
    loadPrefixes();
    hideSpinner();
};

const getPrefixes = async () => {
    showSpinner();
    try {
        const rest = await fetch(`${API_PREFIX}/my/repo/${repoId}/prefixes`, {cache: 'no-store'});
        const data = await rest.json();

        hideSpinner();
        return data;
    } catch (e) {
        console.log(e)
    }
    hideSpinner();
};

const import_exportPrefixes = async () => {
    showSpinner();
    try {
        const prefixes = await getPrefixes();
        console.log(prefixes);
        showModal("Prefix import", `<div class="form-group">
    <label for="prefix_import">Filter JSON:</label>
    <pre contenteditable="true" class="form-control" id="prefix_import" style="height: 50vh">${JSON.stringify(prefixes, null, 2)}</pre>
  </div><button class="btn btn-sm btn-outline-secondary" onclick="putPrefixes(document.getElementById('prefix_import').innerText);">Import JSON</button>
`, true);
    } catch (e) {
        console.log(e)
    }
    hideSpinner();
};

let currentRoute = "";
let currentRouteKey = "";
let defaultRouteKey = "#diffs";
const route = async () => {
    let nextRoute = "";
    let ignoreSameRoute = true;
    switch (window.location.hash) {
        case "":
            window.location.hash = defaultRouteKey;
            return;
        case "#top":
            // Ignore, but restore to routeKey
            window.location.hash = currentRouteKey;
            return;
        case "#prefixes":
            nextRoute = "prefixes";
            break;
        case "#repoChange":
            ignoreSameRoute = false;
            window.location.hash = currentRouteKey;
        // fallthrough
        case "#diffs":
        default:
            nextRoute = "diffs";
            break;
    }

    if (nextRoute === currentRoute && ignoreSameRoute) {
        console.log("Routes identical");
        return
    }

    showSpinner();
    setSpinnerText("Navigating...");
    clearOutput();
    switch (nextRoute) {
        case "prefixes":
            currentRouteKey = "#prefixes";
            document.getElementById("sidebar_prefixes").classList.add("active");
            document.getElementById("sidebar_diffs").classList.remove("active");
            await displayPartial("prefixes");
            await loadPrefixes();
            break;
        case "diffs":
        default:
            currentRouteKey = "#diffs";
            document.getElementById("sidebar_diffs").classList.add("active");
            document.getElementById("sidebar_prefixes").classList.remove("active");
            await displayPartial("diffs");
            await diffSinceLastReadCommit();
            break;
    }
    hideSpinner();
    currentRoute = nextRoute;
};

let spinnerCount = 0;
const hideSpinner = () => {
    spinnerCount--;
    console.log("hideSpinner", spinnerCount);
    if (spinnerCount <= 0) {
        document.getElementById("loading").hidden = true
    }
};

const showSpinner = () => {
    spinnerCount++;
    console.log("showSpinner", spinnerCount);
    document.getElementById("loading").hidden = false
};

const setSpinnerText = (text) => {
    document.getElementById("spinner_text").innerText = text;
    console.log(text);
    feather.replace();
};

const exportUserData = async () => {
    let res = await fetch(`${API_PREFIX}/my/export`, {cache: 'no-store'});
    const user = await res.text();
    showModal("User Data Export", user, false);

}

window.addEventListener("load", app);
