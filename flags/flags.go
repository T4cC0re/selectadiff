package flags

import (
	"flag"
	"github.com/prometheus/common/log"
	"os"
)

var StoragePath = flag.String("storage", "./repo", "Where to store the git repository")
var DatabasePath = flag.String("database", "./db.sqlite", "Where to store the SQLite DB")
var OldGit = flag.Bool("oldgit", false, "This disables some features not available until git 2.29+")
var CacheSize = flag.Int("cache.size", 16, "Amount of user specific change sets to keep in memory. This is useful to prevent repeated expensive git operations")

// The default ClientID is valid for these redirects:
// https://handbookdiff.t4cc0.re/api/oauth/redirect
// http://localhost:8081/api/oauth/redirect
var ClientID = flag.String("oauth.clientid", "c67e3e863dc1cf535354dc645f9916d29819e0fde4e36a526e51c81675c5037e", "This application's GitLab API client ID")
var RedirectURL = flag.String("oauth.redirect", "http://localhost:8081/api/oauth/redirect", "This application's exposed redirect endpoint")

func init() {
	flag.Parse()

	if url := os.Getenv("REDIRECT_URL"); url != "" {
		RedirectURL = &url
	}

	if id := os.Getenv("CLIENT_ID"); id != "" {
		ClientID = &id
	}

	log.Infof("Storage:\t%s\n", *StoragePath)
	log.Infof("DatabasePath:\t%s\n", *DatabasePath)
	log.Infof("oldgit:\t%t\n", *OldGit)
	log.Infof("oauth.redirect:\t%t\n", *RedirectURL)
	log.Infof("oauth.clientid:\t%t\n", *ClientID)
	log.Infof("cache.size:\t%t\n", *CacheSize)
}
