package userdata

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"gitlab.com/T4cC0re/selectadiff/database"
	"gitlab.com/T4cC0re/selectadiff/git"
	"gitlab.com/T4cC0re/selectadiff/util"
	"gorm.io/gorm"
	"strconv"
	"time"
)

type UserData struct {
	GitLabUserId   int64     `json:"gitlab_user_id" gorm:"primaryKey;column:gitlab_user_id"`
	RepoId         int64     `json:"repo_id" gorm:"primaryKey;not null"`
	Repo           *git.Repo `gorm:"primaryKey;foreignKey:RepoId;not null"`
	CreatedAt      time.Time `json:"-"`
	UpdatedAt      time.Time `json:"-"`
	LastCommitHash string    `json:"last_commit"`
	FilterPrefixes []string  `json:"filter_prefixes" gorm:"-"`
	IntPrefixes    string    `json:"-" gorm:"type:blob;column:prefixes"`
}

//func (user *UserData) AfterCreate(tx *gorm.DB) (err error) {
//	if user.LastCommitHash == "" {
//		user.LastCommitHash, _ = git.GetCurrentCommit(*flags.StoragePath)
//	}
//	err = json.Unmarshal([]byte(user.IntPrefixes), &user.FilterPrefixes)
//	if len(user.FilterPrefixes) == 0 {
//		user.FilterPrefixes = []string{"*"}
//	}
//	return
//}

func (user *UserData) AfterFind(tx *gorm.DB) (err error) {
	// If a user was created before we got a repo

	if user.LastCommitHash == "" {
		user.LastCommitHash, err = git.GetCurrentCommit(user.Repo)
	}
	err = json.Unmarshal([]byte(user.IntPrefixes), &user.FilterPrefixes)
	if len(user.FilterPrefixes) == 0 {
		user.FilterPrefixes = []string{"*"}
	}
	return
}

func (user *UserData) BeforeSave(tx *gorm.DB) (err error) {
	var v []byte
	if user.RepoId == 0 {
		return git.ErrNoRepo
	}
	// If a user was created before we got a repo
	if user.LastCommitHash == "" {
		user.LastCommitHash, _ = git.GetCurrentCommit(user.Repo)
	}
	if len(user.FilterPrefixes) == 0 {
		user.FilterPrefixes = []string{"*"}
	}
	v, err = json.Marshal(user.FilterPrefixes)
	user.IntPrefixes = string(v)
	return
}

func (user *UserData) ETag() (etag string) {
	var v []byte
	v, _ = json.Marshal(user.FilterPrefixes)
	hash := sha256.Sum256(v)
	return "\"" + hex.EncodeToString(hash[:]) + "\""
}

func GetOrCreateUserData(userId string, repo *git.Repo) (user *UserData, err error) {
	id := util.MustInt64(strconv.ParseInt(userId, 10, 64))

	userData := UserData{}

	if repo == nil {
		return nil, git.ErrNoRepo
	}

	tx := database.DB.Preload("Repo").FirstOrCreate(&userData, UserData{GitLabUserId: id, RepoId: repo.ID, Repo: repo})
	if tx.Error != nil {
		err = tx.Error
		return
	}
	return &userData, nil
}
