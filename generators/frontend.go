// +build ignore

package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"github.com/go-bindata/go-bindata/v3"
	"github.com/prometheus/common/log"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func main() {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	c := bindata.NewConfig()
	c.NoMemCopy = true
	c.Output = path.Join(wd, "frontend.go")
	c.HttpFileSystem = true
	if os.Getenv("DEBUG") != "1" {
		// Generate random asset IDs for each build or use $pkgver
		var assetId string
		if os.Getenv("pkgver") != "" {
			assetId = os.Getenv("pkgver")
		} else {
			h := sha256.New()
			buf := make([]byte, 512)
			n, _ := rand.Read(buf)
			if n != len(buf) {
				panic(io.ErrShortBuffer)
			}
			h.Write(buf)
			assetId = fmt.Sprintf("build-%x", h.Sum(nil))[0:14]
		}
		fmt.Println(assetId)

		dir, err := ioutil.TempDir("", fmt.Sprintf("selectadiff_frontend_%s", assetId))
		if err != nil {
			panic(err)
		}
		defer os.RemoveAll(dir)

		matches, _ := filepath.Glob("frontend/*")

		assets := map[string]string{}

		for _, file := range matches {
			if !strings.HasSuffix(file, ".html") {
				// Alter filename
				ext := filepath.Ext(file)
				newFile := strings.ReplaceAll(file, ext, "_"+assetId+ext)
				newPath := path.Join(dir, newFile)

				err := os.MkdirAll(filepath.Dir(newPath), 0777)
				if err != nil {
					panic(err)
				}

				// Copy file
				data, err := ioutil.ReadFile(file)
				if err != nil {
					panic(err)
				}
				err = ioutil.WriteFile(path.Join(dir, newFile), data, 0666)
				if err != nil {
					panic(err)
				}

				// Add to potentials asset list
				assets[path.Base(file)] = path.Base(newFile)
			}
		}

		for _, file := range matches {
			if strings.HasSuffix(file, ".html") {
				// Alter filename
				newPath := path.Join(dir, file)

				err := os.MkdirAll(filepath.Dir(newPath), 0777)
				if err != nil {
					panic(err)
				}

				// Replace assets
				data, err := ioutil.ReadFile(file)
				if err != nil {
					panic(err)
				}

				for source, target := range assets {
					data = bytes.ReplaceAll(data, []byte(source), []byte(target))
				}
				data = bytes.ReplaceAll(data, []byte("$pkgver"), []byte(assetId))

				err = ioutil.WriteFile(path.Join(dir, file), data, 0666)
				if err != nil {
					panic(err)
				}
			}
		}

		fmt.Printf("%+v\n", assets)
		fmt.Printf("%+v\n", matches)

		err = os.Chdir(dir)
		if err != nil {
			panic(err)
		}

		log.Info(dir)

		matches, _ = filepath.Glob("frontend/*")
		fmt.Printf("%+v\n", matches)
	}

	c.Prefix = "frontend"
	c.Debug = os.Getenv("DEBUG") == "1"
	c.Input = []bindata.InputConfig{
		{
			Path:      filepath.Clean("frontend"),
			Recursive: true,
		},
	}

	err = bindata.Translate(c)
	if err != nil {
		panic(err)
	}
}
