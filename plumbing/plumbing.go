package plumbing

import (
	"bufio"
	"crypto/sha256"
	"fmt"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/selectadiff/git"
	"gitlab.com/T4cC0re/selectadiff/userdata"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Change struct {
	File         string `json:"file"`
	ChangeSetID  string `json:"-"`
	ChangeType   string `json:"-"`
	Deleted      bool   `json:"deleted,omitempty"`
	Added        bool   `json:"added,omitempty"`
	Modified     bool   `json:"modified,omitempty"`
	TypeChange   bool   `json:"type_change,omitempty"`
	RenameTarget string `json:"rename_target,omitempty"`
}

type ChangeSet struct {
	ID        string `json:"diff_id"`
	CreatedAt time.Time
	Count     int       `json:"count"`
	Commit    string    `json:"from_commit"`
	Target    string    `json:"to_commit"`
	Repo      *git.Repo `json:"-"`
	Changes   []*Change `json:"changes,omitempty"`
}

type cache struct {
	content []*ChangeSet
	limit   int
	sync.RWMutex
}

var ChangeCache cache

func (c *cache) Reset(limit int) {
	c.Lock()
	defer c.Unlock()
	c.limit = limit
	c.content = make([]*ChangeSet, 0, limit)
}

func (c *cache) Get(diffId string) *ChangeSet {
	c.RLock()
	defer c.RUnlock()
	for _, cs := range c.content {
		if cs != nil && cs.ID == diffId {
			return cs
		}
	}
	return nil
}

func (c *cache) Delete(diffId string) {
	c.Lock()
	defer c.Unlock()
	for idx, cs := range c.content {
		if cs != nil && cs.ID == diffId {
			c.content[idx] = nil
			return
		}
	}
}

func (c *cache) Insert(changeSet *ChangeSet) {
	c.Lock()
	defer c.Unlock()
	if c.limit == 0 {
		c.limit = 16
	}
	changeSet.CreatedAt = time.Now()
	if len(c.content) >= c.limit {
		// Find and evict oldest.
		var oldestIndex int
		var oldestTime time.Time = time.Now()
		for idx, cs := range c.content {
			if cs == nil {
				// This is nil, we can use this index
				c.content[idx] = changeSet
				return
			}
			if cs.CreatedAt.Before(oldestTime) {
				oldestTime = cs.CreatedAt
				oldestIndex = idx
			}
		}
		c.content[oldestIndex] = changeSet
		return
	}
	c.content = append(c.content, changeSet)
}

type UserChangeSet struct {
	UserId   int64 `json:"user_id,omitempty"` // This is for filtered results
	Filtered bool  `json:"filtered"`
	ChangeSet
}

func getDiffId(hash string, toCommit string) string {
	h := sha256.New224()
	h.Write([]byte(fmt.Sprintf("%s..%s", hash, toCommit)))
	return fmt.Sprintf("%x", h.Sum(nil))
}

func ChangesFromCommit(repo *git.Repo, fromCommit string, toCommit string) (changeset *ChangeSet, cacheHit bool, err error) {
	if repo == nil {
		return nil, false, git.ErrNoRepo
	}
	fromCommit = strings.TrimSpace(fromCommit)
	toCommit = strings.TrimSpace(toCommit)

	if !git.ValidCommithash.MatchString(fromCommit) {
		return nil, false, git.ErrInvalidHash
	}
	if toCommit == "" {
		return nil, false, git.ErrInvalidHash
	}
	diffId := getDiffId(fromCommit, toCommit)

	if ccs := ChangeCache.Get(diffId); ccs != nil {
		log.With("diffId", diffId).Infof("diffId served from ChangeCache.")
		return ccs, true, nil
	}
	log.With("diffId", diffId).Infof("diffId not in ChangeCache. Fetching...")

	cs := ChangeSet{
		Commit: fromCommit,
		Target: toCommit,
		Repo:   repo,
		ID:     diffId,
	}

	if toCommit == fromCommit {
		// No need to exec anything if we compare the same thing
		log.With("diffId", diffId).Infof("Comparing the same revisions.")
		return &cs, true, nil
	}

	params := []string{
		"-C", repo.Path,
		"-c", "diff.renameLimit=999999",
		"diff",
		"--name-status",
		"--full-history",
		"--simplify-merges",
		fmt.Sprintf("%s..%s", fromCommit, toCommit),
	}

	cmd := git.Command(params...)
	log.With("diffId", diffId).Infof("executing '%s'", cmd.String())
	r, _ := cmd.StdoutPipe()
	cmd.Stderr = os.Stderr
	done := make(chan struct{})
	scanner := bufio.NewScanner(r)

	go func() {
		for scanner.Scan() {
			change := parseChange(scanner.Text(), diffId)
			if change != nil {
				cs.Changes = append(cs.Changes, change)
			}
		}
		done <- struct{}{}
	}()

	err = cmd.Start()
	if err != nil {
		log.Error(err)
		return
	}

	<-done
	err = cmd.Wait()
	if err != nil {
		log.Error(err)
		return
	}

	cs.Count = len(cs.Changes)

	log.
		With("Count", cs.Count).
		With("Commit", cs.Commit).
		With("Target", cs.Target).
		With("Repo", cs.Repo).Infoln("parsed")

	ChangeCache.Insert(&cs)

	changeset = &cs
	return
}

func DiffFromChanges(changeSet *ChangeSet, count int, offset int, w http.ResponseWriter, url *url.URL) (err error) {
	if !git.ValidCommithash.MatchString(changeSet.Commit) {
		w.WriteHeader(http.StatusBadRequest)
		return git.ErrInvalidHash
	}

	var files []string

	if count > 50 || count == 0 {
		count = 50
	}

	if len(changeSet.Changes) < offset {
		w.WriteHeader(http.StatusNoContent)
		return nil
	}
	if len(changeSet.Changes) < count+offset {
		count = len(changeSet.Changes) - offset
	}
	if count == 0 {
		w.WriteHeader(http.StatusNoContent)
		return nil
	}

	nextOffset := offset + count
	if nextOffset != len(changeSet.Changes) {
		q := url.Query()
		q.Set("offset", strconv.Itoa(nextOffset))
		url.RawQuery = q.Encode()
		w.Header().Set("X-Next", url.String())
	}

	log.Infof("count: %d, offset %d", count, offset)

	for _, change := range changeSet.Changes[offset:nextOffset] {
		if change.RenameTarget != "" {
			files = append(files, change.RenameTarget)
		} else {
			files = append(files, change.File)
		}
	}
	if len(files) > 0 {
		w.Header().Set("Content-Type", "text/x-diff; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		err = git.GitDiff(changeSet.Repo, changeSet.Commit, changeSet.Target, files, w)
		return err
	} else {
		w.WriteHeader(http.StatusNoContent)
		return err
	}
}

func parseChange(line string, diffId string) (change *Change) {
	line = strings.TrimSpace(line)
	splits := strings.Split(line, "\t")
	if len(splits) < 2 {
		return nil
	}

	change = &Change{
		ChangeSetID: diffId,
		ChangeType:  splits[0],
		File:        splits[1],
	}

	switch splits[0] {
	case "A":
		change.Added = true
	case "D":
		change.Deleted = true
	case "T":
		change.TypeChange = true
	case "M":
		change.Modified = true
	default:
		if strings.HasPrefix(splits[0], "R") && len(splits) == 3 {
			change.RenameTarget = splits[2]
		} else {
			log.With("diffId", diffId).Errorf("Cannot parse change type %s in %s\n", splits[0], line)
			return nil
		}
	}
	return
}

func FilterChangeset(userData *userdata.UserData, in_changeSet *ChangeSet) (changeSet *UserChangeSet, err error) {
	changeSet = &UserChangeSet{
		UserId:   userData.GitLabUserId,
		Filtered: true,
		ChangeSet: ChangeSet{
			Count:   0,
			Commit:  in_changeSet.Commit,
			Target:  in_changeSet.Target,
			Repo:    in_changeSet.Repo,
			Changes: nil,
		},
	}

	var tempChanges = make([]*Change, 0, len(in_changeSet.Changes))

	// add everything that matches
	for _, prefix := range userData.FilterPrefixes {
		if strings.HasPrefix(prefix, "!") {
			continue
		}
		prefix = strings.TrimLeft(prefix, "/")

		for _, change := range in_changeSet.Changes {
			if prefix == "*" || strings.HasPrefix(change.File, prefix) || strings.HasPrefix(change.RenameTarget, prefix) {
				tempChanges = append(tempChanges, change)
			}
		}
	}

	// Remove negated matches from cadidates
	for _, prefix := range userData.FilterPrefixes {
		if !strings.HasPrefix(prefix, "!") {
			continue
		}
		prefix = strings.TrimLeft(prefix, "!/")

		for idx, change := range tempChanges {
			if change == nil {
				continue
			}
			if strings.HasPrefix(change.File, prefix) || strings.HasPrefix(change.RenameTarget, prefix) {
				tempChanges[idx] = nil
			}
		}
	}

	// Finalize and remove nil values
	for _, item := range tempChanges {
		if item != nil {
			changeSet.Changes = append(changeSet.Changes, item)
		}
	}

	changeSet.Count = len(changeSet.Changes)

	return
}
