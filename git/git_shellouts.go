package git

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"github.com/prometheus/common/log"
	"gitlab.com/T4cC0re/selectadiff/database"
	"gitlab.com/T4cC0re/selectadiff/flags"
	"gitlab.com/T4cC0re/selectadiff/util"
	"io"
	"os"
	"os/exec"
	path2 "path"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var ValidCommithash = regexp.MustCompile(`(?i)^[0-9a-f]{8,}$`)
var ErrInvalidHash = errors.New("invalid commit hash")
var ErrNoCommit = errors.New("no commit found")
var ErrNoRepo = errors.New("no repo found")
var commonFlags = []string{
	"-c", "pager.diff=false",
	"-c", "core.autocrlf=input",
	"-c", "core.commitGraph=true",
	"-c", "core.fsyncObjectFiles=true",
	"-c", "gc.auto=0",
	"-c", "gc.writeCommitGraph=true",
}

type Commit struct {
	Hash          string `json:"hash"`
	AuthorName    string `json:"author_name"`
	AuthorEmail   string `json:"author_email"`
	AuthoringTime int64  `json:"authoring_time"`
	Subject       string `json:"subject"`
	Body          string `json:"body"`
}

type Repo struct {
	ID             int64     `gorm:"primaryKey" json:"id"`
	CreatedAt      time.Time `json:"-"`
	UpdatedAt      time.Time `json:"-"`
	Name           string    `json:"name"`
	Path           string    `gorm:"uniqueIndex" json:"-"`
	Upstream       string    `gorm:"uniqueIndex" json:"upstream"`
	TrackingBranch string    `json:"tracking_branch"`
}

func GetRepoByID(repoID string) (repo *Repo) {
	repoId := util.MustInt64(strconv.ParseInt(repoID, 10, 64))

	repoO := Repo{}

	tx := database.DB.First(&repoO, Repo{ID: repoId})
	if tx.Error != nil || repoO.ID == 0 {
		log.Errorf("Could not get repo ID %d", repoId)

		return nil
	}
	return &repoO
}

func Command(args ...string) *exec.Cmd {
	args = append(commonFlags, args...)
	return exec.Command("git", args...)
}

func DetectDefaultBranch(upstream string) (defaultBranch string, err error) {
	var re = regexp.MustCompile(`(?m)ref:\srefs/heads/(\S+)\sHEAD`)
	cmd := Command("ls-remote", "--symref", upstream, "HEAD")

	var out []byte
	out, err = cmd.Output()
	if err != nil {
		return
	}

	if match := re.FindStringSubmatch(string(out)); len(match) > 1 {
		defaultBranch = match[1]
	}

	if defaultBranch == "" {
		err = ErrNoRepo
	}
	return
}

func WriteCommitGraph(repo *Repo, w io.Writer) (err error) {
	if repo == nil {
		return ErrNoRepo
	}
	params := []string{
		"-C", repo.Path, "commit-graph", "write",
	}

	// Allow disabling of features only present in newer versions (2.29+)
	if !(*flags.OldGit) {
		params = append(params, "--reachable", "--changed-paths")
	}

	cmd := Command(params...)
	log.Infoln(cmd.String())

	cmd.Stdin = os.Stdin
	cmd.Stderr = w
	cmd.Stdout = w
	err = cmd.Run()
	return
}

func GetCurrentCommit(repo *Repo) (string, error) {
	if repo == nil {
		return "", ErrNoRepo
	}
	cmd := Command("-C", repo.Path, "rev-parse", "--verify", repo.TrackingBranch)
	commit, err := cmd.Output()
	if err != nil {
		return "", err
	}
	if string(commit) == "" {
		return "", ErrNoCommit
	}
	return strings.TrimSpace(string(commit)), nil
}

func GetFirstCommit(repo *Repo) (string, error) {
	log.Info(repo)
	if repo == nil {
		return "", ErrNoRepo
	}
	cmd := Command("-C", repo.Path, "rev-list", "--max-parents=0", "--date-order", "--reverse", repo.TrackingBranch)
	commit, err := cmd.Output()
	if err != nil {
		return "", err
	}
	log.Info(commit)
	if string(commit) == "" {
		return "", ErrNoCommit
	}
	commit = bytes.Split(commit, []byte("\n"))[0]
	return strings.TrimSpace(string(commit)), nil
}

func CommitByDate(repo *Repo, date time.Time) (string, error) {
	if repo == nil {
		return "", ErrNoRepo
	}
	cmd := Command("-C", repo.Path, "rev-list", "-n", "1", "--first-parent", fmt.Sprintf("--before=%s", date.Format(time.RFC3339)), repo.TrackingBranch)
	commit, err := cmd.Output()
	if err != nil {
		return "", err
	}
	if string(commit) == "" {
		return "", ErrNoCommit
	}
	return strings.TrimSpace(string(commit)), nil
}

func GitDiff(repo *Repo, commit string, toCommit string, files []string, w io.Writer) (err error) {
	if repo == nil {
		return ErrNoRepo
	}
	if len(files) == 0 {
		return
	}
	params := []string{
		"-C", repo.Path,
		"-c", "diff.renameLimit=999999",
		"diff",
		"--patch",
		"--full-history",
		"--simplify-merges",
		"--ignore-all-space",
		commit + ".." + toCommit,
		"--",
	}
	params = append(params, files...)

	cmd := Command(params...)
	log.Infoln(cmd.String())

	//cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Stdout = w
	err = cmd.Run()
	return
}

func GitPull(repo *Repo, w io.Writer) (err error) {
	if repo == nil {
		return ErrNoRepo
	}
	params := []string{
		"-C", repo.Path,
		"remote",
		"update",
		"--prune",
	}

	cmd := Command(params...)
	cmd.Env = append(cmd.Env, "GIT_CURL_VERBOSE=1", "GIT_TRACE=1")
	log.Infoln(cmd.String())

	//cmd.Stdin = os.Stdin
	cmd.Stderr = w
	cmd.Stdout = w
	err = cmd.Run()
	return
}

func GitClone(repo *Repo, w io.Writer) (err error) {
	if repo == nil {
		return ErrNoRepo
	}
	params := []string{
		"clone",
		"--verbose",
		"--mirror",
		"--progress",
		"--no-tags",
		repo.Upstream,
		repo.Path,
	}

	cmd := Command(params...)
	cmd.Env = append(cmd.Env, "GIT_CURL_VERBOSE=1", "GIT_TRACE=1")
	log.Infoln(cmd.String())

	//cmd.Stdin = os.Stdin
	cmd.Stderr = w
	cmd.Stdout = w
	err = cmd.Run()
	return
}

func CommitDetails(repo *Repo, commitHash string) (commit *Commit, err error) {
	if repo == nil {
		return nil, ErrNoRepo
	}
	commitHash = strings.TrimSpace(commitHash)
	cmd := Command("-C", repo.Path, "show", "--format=%H%x00%an%x00%ae%x00%at%x00%s%x00%b", commitHash)
	rawCommit, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	if string(rawCommit) == "" {
		return nil, ErrNoCommit
	}
	splits := bytes.SplitN(rawCommit, []byte{0x00}, 6)
	if len(splits) != 6 {
		return nil, ErrNoCommit
	}
	commit = &Commit{
		Hash:          string(splits[0]),
		AuthorName:    string(splits[1]),
		AuthorEmail:   string(splits[2]),
		AuthoringTime: util.MustInt64(strconv.ParseInt(string(splits[3]), 10, 64)),
		Subject:       string(splits[4]),
		Body:          strings.TrimSpace(string(splits[5])),
	}
	return commit, nil
}

type DirList struct {
	Dirs  []string `json:"dirs"`
	Files []string `json:"files"`
}

var ErrMalformed = errors.New("malformed line")

func List(repo *Repo, origPath string) (dirList *DirList, err error) {
	if repo == nil {
		return nil, ErrNoRepo
	}
	origPath = strings.TrimSpace(origPath)
	path := path2.Clean(origPath)
	if strings.HasSuffix(origPath, "/") {
		path += "/"
	}
	dirList = &DirList{}

	log.Info(path)

	params := []string{
		"-C", repo.Path,
		"ls-tree",
		"--full-tree",
		repo.TrackingBranch,
		"--",
		path,
	}

	cmd := Command(params...)
	r, _ := cmd.StdoutPipe()
	cmd.Stderr = os.Stderr
	scanner := bufio.NewScanner(r)

	err = cmd.Start()
	if err != nil {
		log.Error(err)
		return nil, err
	}

	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.SplitN(line, "\t", 2)
		if len(parts) != 2 {
			log.Errorf("Malformed output: '%s': %+v", line, parts)
			return nil, ErrMalformed
		}
		attributes := strings.SplitN(parts[0], " ", 3)
		if len(attributes) != 3 {
			log.Errorf("Malformed output: '%s': %+v", parts[0], attributes)
			return nil, ErrMalformed
		}
		switch attributes[1] {
		case "tree":
			dirList.Dirs = append(dirList.Dirs, path2.Base(parts[1]))
		default:
			dirList.Files = append(dirList.Files, path2.Base(parts[1]))
		}
	}

	err = cmd.Wait()
	if err != nil {
		log.Error(err)
		return nil, err
	}

	// We are listing a directory, but the requestor forgot the trailing slash.
	if len(dirList.Dirs) == 1 && dirList.Dirs[0] == path2.Base(path) {
		return List(repo, path+"/")
	}

	return
}
