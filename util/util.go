package util

import (
	"encoding/json"
	"github.com/prometheus/common/log"
	"net/http"
)

func WriteJSON(w http.ResponseWriter, payload interface{}, pretty bool) {
	var buf []byte
	var err error
	if pretty {
		buf, err = json.MarshalIndent(payload, "", "  ")
	} else {
		buf, err = json.Marshal(payload)
	}
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(buf)
}

func MustInt64(i int64, _ error) int64 {
	return i
}
