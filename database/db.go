package database

import (
	"gitlab.com/T4cC0re/selectadiff/flags"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"os"
	"time"

	stdLog "log"
)

var DB *gorm.DB

func Open() (err error) {
	newLogger := logger.New(
		stdLog.New(os.Stdout, "\r\n", stdLog.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,        // Disable color
		},
	)

	DB, err = gorm.Open(sqlite.Open(*flags.DatabasePath), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
		Logger:                                   newLogger,
	})
	DB.Exec("PRAGMA journal_mode=WAL")
	DB.Exec("PRAGMA synchronous=NORMAL")
	Sync()
	return
}

func Sync() {
	DB.Exec("PRAGMA wal_checkpoint(TRUNCATE)")
	DB.Exec("PRAGMA optimize")
	DB.Exec("VACUUM")
}
